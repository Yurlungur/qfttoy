(*
   minkowski_qft_2d.m
   Time-stamp: <2014-04-18 00:28:08 (jonah)>
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

   This little library contains the code for a quantum field in 2d in
   flat Minkowski space.
   *)

(* Load requisite modules*)
Get["SHO.m"];

(* Sample the mode function for every mode number Needs the mass m,
   the box size L, and the max mode number. *)
ModeSampler2d[m_,L_,kmax_]:=Block[{n=0,dist},
				  dist[kx_,ky_]:=RandomVariate[MakeShoProbability[n,m,ShoFrequency[m,L,kx,ky]]];
				  Evaluate[Table[dist[i,j],{i,1,kmax},{j,1,kmax}]]]
				 

(* Given a mass m and a box length L, generates quantum fluctuations
   for a 2d field in the ground state. We must also specify the max mode
   number to sum over. kmax is the max mode number.*)
MakeFluctuations2dGS[m_,L_,kmax_]:=With[{n=0,
					 phiK=Evaluate[ModeSampler2d[m,L,kmax]]},
					Function[{x,y},
						 Evaluate[Sum[phiK[[kx,ky]]\
							      *Sin[kx*Pi*x/L]\
							      *Sin[ky*Pi*y/L],
							      {kx,1,kmax},
							      {ky,1,kmax}]]]];

(* Given a mass m, a box length L, and a max mode number kmax,
   generates quantum fluctuations for a 2d field in the ground state,
   then finds the varianve and plots the field and the square of the
   field. *)
ComputeFluctuations2dGS[m_,L_,kmax_]:=Module[{flucts,variance,
					      plot,squareplot},
					     flucts=MakeFluctuations2dGS[m,L,kmax];
					     variance=NIntegrate[flucts[x,y]^2,
								 {x,0,L},
								 {y,0,L},
								 Method->{"AdaptiveMonteCarlo",
									  "SymbolicProcessing"->False}];
					     plot=Plot3D[flucts[x,y],
							 {x,0,L},{y,0,L},
							 PlotRange->All];
					     squareplot=Plot3D[flucts[x,y]^2,
							       {x,0,L},{y,0,L},
							       PlotRange->All];
					     {flucts,variance,plot,squareplot}]

(* Given a mass m, a box length L, and a max mode number kmax,
   calculates the variance for the quantum fluctuations of a 2d field in
   the ground state averaged over numMeasurements measurements. *)
ComputeAverageVariance2dGS[m_,L_,kmax_,numMeasurements_]:=Module[{average,stdDev,variances},
								 variances=Table[Module[{flucts,tempVariance},
											flucts=MakeFluctuations2dGS[m,L,kmax];
											tempVariance=NIntegrate[flucts[x,y]^2,
														{x,0,L},
														{y,0,L},
														Method->{"AdaptiveMonteCarlo",
															 "SymbolicProcessing"->False}];
											tempVariance],
										 {numMeasurements}];
								 average=Mean[variances];
								 stdDev=StandardDeviation[variances];
								 {average,stdDev}]

