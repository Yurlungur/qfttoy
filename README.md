qfttoy: A mathematica script to play with scalar fields 
======================================================================
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-04-22 18:42:11 (jonah)>

                         INTRODUCTION
======================================================================

This is my little Mathematica script to sample the amplitude of a
massive scalar field in Minkowski spacetime and in inflationary
Friedmann-Lemaitre-Roberstson Walker spacetimes.

For an explanation of the physics see the included pdf file.

                    INSTALLATION and USE
======================================================================

To install, clone the code directory and open ui.nb in
Mathematica. Change the "directory" name to your directory and
evaluate the notebook. Evaluating the entire notebook could take a
very long time, perhaps a day or more, so be ready for that.

The mathematica code in the *.m files is plain-text and well
commented.