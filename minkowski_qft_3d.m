(*
   minkowski_qft_3d.m
   Time-stamp: <2014-04-19 15:56:10 (jonah)>
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

   This little library contains the code for a quantum field in 3d in
   flat Minkowski space.
   *)

(* Load requisite modules*)
Get["SHO.m"];

(* Sample the mode function for every mode number Needs the mass m,
   the box size L, and the max mode number. *)
ModeSampler3d[m_,L_,kmax_]:=Block[{n=0,dist},
				  dist[kx_,ky_,kz_]:=RandomVariate[MakeShoProbability[n,m,ShoFrequency[m,L,kx,ky,kz]]];
				  Evaluate[Table[dist[i,j,k],{i,1,kmax},{j,1,kmax},{k,1,kmax}]]]

(* Sample the mode function for every mode number Needs the mass m,
   the box size L, and the max mode number. However, let the
   (kx0,ky0,kz0) mode be in the n^th excited state and let all other
   modes be in the ground state. *)
ModeSampler3d[m_,L_,kmax_,kx0_,ky0_,kz0_,n_]:=Block[{n0=0,dist},
						    dist[kx_,ky_,kz_]:=RandomVariate[MakeShoProbability[If[{kx,ky,kz}=={kx0,ky0,kz0},n,n0],
													m,ShoFrequency[m,L,kx,ky,kz]]];
						    Evaluate[Table[dist[i,j,k],{i,1,kmax},{j,1,kmax},{k,1,kmax}]]]
						    

(* Given a mass m and a box length L, generates quantum fluctuations
   for a 3d field in the ground state. We must also specify the max mode
   number to sum over. kmax is the max mode number.*)
MakeFluctuations3dGS[m_,L_,kmax_]:=With[{n=0,
					 phiK=Evaluate[ModeSampler3d[m,L,kmax]]},
					Function[{x,y,z},
						 Evaluate[Sum[phiK[[kx,ky,kz]]\
							      *Sin[kx*Pi*x/L]\
							      *Sin[ky*Pi*y/L]\
							      *Sin[kz*Pi*z/L],
							      {kx,1,kmax},
							      {ky,1,kmax},
							      {kz,1,kmax}]]]];

(* Given a mass m and a box length L, generates quantum fluctuations
   for a 3d field. The (kx0,ky0,kz0) mode is in the nth excited
   state. All other modes are in the ground state. We must also
   specify the max mode number to sum over. kmax is the max mode
   number.*)
MakeFluctuations3dGS[m_,L_,kmax_,kx0_,ky0_,kz0_,n_]:=With[{phiK=Evaluate[ModeSampler3d[m,L,kmax,kx0,ky0,kz0,n]]},
							  Print[Evaluate[phiK[[kx0,ky0,kz0]]]];
							  Function[{x,y,z},
								   Evaluate[Sum[phiK[[kx,ky,kz]]\
										*Sin[kx*Pi*x/L]\
										*Sin[ky*Pi*y/L]\
										*Sin[kz*Pi*z/L],
										{kx,1,kmax},
										{ky,1,kmax},
										{kz,1,kmax}]]]];
(* Given a mass m, a box length L, and a max mode number kmax,
   generates quantum fluctuations for a 3d field in the ground state,
   then finds the varianve and plots the field and the square of the
   field at z=zpercent*L. *)
ComputeFluctuations3dGS[m_,L_,kmax_,zpercent_]:=Module[{flucts,variance,
							plot,squareplot},
						       flucts=MakeFluctuations3dGS[m,L,kmax];
						       variance=NIntegrate[flucts[x,y,z]^2,
									   {x,0,L},
									   {y,0,L},
									   {z,0,L},
									   Method->{"AdaptiveMonteCarlo",
										    "SymbolicProcessing"->False}];
						       plot=Plot3D[flucts[x,y,zpercent*L],
								   {x,0,L},{y,0,L},
								   PlotRange->All];
						       squareplot=Plot3D[flucts[x,y,zpercent*L]^2,
									 {x,0,L},{y,0,L},
									 PlotRange->All];
						       {flucts,variance,plot,squareplot}]

(* Given a mass m, a box length L, and a max mode number kmax,
   calculates the variance for the quantum fluctuations of a 3d field in
   the ground state averaged over numMeasurements measurements. *)
ComputeAverageVariance3dGS[m_,L_,kmax_,numMeasurements_]:=Module[{average,stdDev,variances},
								 variances=Table[Module[{flucts,tempVariance},
											flucts=MakeFluctuations3dGS[m,L,kmax];
											tempVariance=NIntegrate[flucts[x,y,z]^2,
														{x,0,L},
														{y,0,L},
														{z,0,L},
														Method->{"AdaptiveMonteCarlo",
															 "SymbolicProcessing"->False}];
											tempVariance],
										 {numMeasurements}];
								 average=Mean[variances];
								 stdDev=StandardDeviation[variances];
								 {average,stdDev}]

