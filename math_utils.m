(* math_utils.m
   Time-stamp: <2014-04-20 23:45:35 (jonah)>

   Several utilities borrowed from David Curtin.

   Web site: http://insti.physics.sunysb.edu/~curtin/mathematica.html

   *)


(* Prevents kernel bloat *)
ClearMemory := Module[{},
		      Unprotect[In, Out];
		      Clear[In, Out];
		      Protect[In, Out];
		      ClearSystemCache[];
		     ];

(* Saves mathematica output to a file as a string. *)
SaveIt[filename_, expr_] := Module[{output},
           output = 
           Export[filename <> ".dat", ToString[expr // InputForm], 
		  "String"];
           ClearMemory;
           output
           ];
SaveIt[varnamestring_] := Module[{output},
           output = 
           Export[varnamestring <> ".dat", 
           ToString[ToExpression[varnamestring] // InputForm],
		  "String"];
           ClearMemory;
           output
           ];
(* Loads output *)
ReadIt[filename_] := Module[{output},
           output = 
           ToExpression[
           Import[StringReplace[filename, ".dat" -> ""] <> ".dat", 
		  "String"]];
           ClearMemory;
           output
           ];
