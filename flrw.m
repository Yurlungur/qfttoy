(*
   flrw.m
   Time-stamp: <2014-04-21 21:56:53 (jonah)>
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

   This little module contains functions for doing quantum field
   theory on Friedmann-Robtertson-Walker (FLRW) spacetimes.

   For convenience, we work in natural units.
   ======================================================================
   *)

(* Load prerequisite modules *)
Get["SHO.m"];
Get["rejection_sampling.m"];
Get["math_utils.m"];

(* Global constants *)
chimax=100;
chimin=-chimax;
normPsiMin=0;
normPsiMax=3; (* just to be safe *)

(* Some basic stuff *)
(* ====================================================================== *)

(* The scale factor for a de Sitter universe, where H is the
   cosmological horizon. *)
scaleFactor[H_,t_]:=Exp[H*t];
(*scaleFactor=Compile[{{H,_Real},{t,_Real}},scaleFactorRaw[H,t]]*)

(* The scale factor using conformal time eta *)
scaleFactorConformal[H_,eta_]:=-1/(H*eta);
(*scaleFactorConformal=Compile[{{H,_Real},{eta,_Real}},
			     scaleFactorConformalRaw[H,eta]];*)

(* Conformal time as a function of scale factor and cosmological horizon. *)
conformalTime[H_,a_]:=-1/(a*H);
(*conformalTime=Compile[{{H,_Real},{a,_Real}},conformalTimeRaw[H,a]];*)

(* The square of the frequency of the chi field (using conformal time)
   in de Sitter space.  Requires knowledge of the length of the box L,
   the mass of the field m, mode vector (kx,ky,kz), the cosmological
   horizon H, and the conformal time eta. *)
chiOmega2[L_,m_,
	  kx_,ky_,kz_,
	  H_,eta_]:=(Pi/L)^2*(kx^2+ky^2+kz^2)+(1/eta^2)*((m/H)^2-2);
(*chiOmega2=Compile[{{L,_Real},{m,_Real},
		   {kx,_Real},{ky,_Real},{kz,_Real},
		   {H,_Real},{eta,_Real}},
		  chiOmega2Raw[L,m,kx,ky,kz,H,eta]];*)

(* If you insist on using the frequency and trust that it's a real
   number, you can use this function. *)
chiFrequency[L_,m_,kx_,ky_,kz_,H_,eta_]:=Sqrt[chiOmega2[L,m,kx,ky,kz,H,eta]];

(* Extract the field amplitude phi given a measurement of chi. We also
   need to know cosmological horizon scale H and the conformal time eta. *)
phiExtracted[H_,eta_,chi_]:=-H*eta*chi;
(*phiExtracted=Compile[{{H,_Real},{eta,_Real},{chi,_Real}},
		     phiExtractedRaw[H,eta,chi]];*)
(* ====================================================================== *)




(* We'd like to be able to extract the probability distribution for
the amplitude of a chi mode at any time, even after the potential
starts wobbling and inverting. As such we define a numericla
regimen. The wavefunction is always called psi. *)
(* ====================================================================== *)

(* The potential energy for a chi field. We need to know the box
length L, the mass m, the wave vector (kx,ky,kz), the cosmological
horizon H, the conformal time eta, and of course chi. *)
vChi[L_,m_,
     kx_,ky_,kz_,
     H_,eta_,chi_]:=(1/2)*chiOmega2[L,m,kx,ky,kz,H,eta]*chi^2;

(* The Schrodinger wave equation. Set this equal to zero. We need to
know the box length L, the mass m, the wave vector (kx,ky,kz), the
cosmological horizon H, the conformal time eta, and of course chi. *)
schrodinger[L_,m_,
	    kx_,ky_,kz_,H_,
	    psi_,t_,chi_] := I*D[psi[t,chi],t]+(1/2)*D[psi[t,chi],chi,chi]\
    - vChi[L,m,kx,ky,kz,H,eta,chi]*psi[t,chi];

(* The initial state we feed into the Schrodinger equation for
chi. For convenience, we assume that at the initial conformal time,
the scale factor is unity. Thus the lowest energy chi state is also
the lowest energy phi state. *)
initialState[L_,m_,
	     kx_,ky_,kz_,
	     H_,chi_,eta_]:=ShoEigenstate[chi,0,m,
					  chiFrequency[L,m,kx,ky,kz,H,eta]];


(* Solves the Schrodinger equation for the wavefunction of a given chi
   mode. We need to know the box size L, the mass m, the mode vector
   (kx,ky,kz), the horizon H, and the variable name chi. etaMax is the
   maximum time we integrate to. Sadly this must be user defined. *)
evolveMode[L_,m_,
	   kx_,ky_,kz_,
	   H_,etamax_]:=Block[{chi,psi,eta},
			      Module[{a0=1,eta0,psi0,
				      sys,soln},
				     eta0=conformalTime[H,a0];
				     psi0=initialState[L,m,
						       kx,ky,kz,
				       H,chi,eta0];
				     sys={schrodinger[L,m,
						      kx,ky,kz,
						      H,psi,eta,chi]==0,
					  psi[eta0,chi]==psi0,
					  psi[eta,chimin]==psi[eta,chimax]};
				     soln=NDSolve[sys,psi,
						  {eta,eta0,etamax},
						  {chi,chimin,chimax},
						  Method->{"MethodOfLines",
			   "SpatialDiscretization"->{"TensorProductGrid",
						     "MinPoints"->2000}},
						  Method->"StiffnessSwitching",
						 PrecisionGoal->20,
						 MaxSteps->25000];
				     soln]];
				     
			 
(* Generates a probability distribution based on evolving a
   mode so that we can sample from it. *)
makeProbDist[L_,m_,
	     kx_,ky_,kz_,
	     H_,etamax_]:=Block[{psi,chi},
            	    Module[{solnRules,nsolnRules,etaFinal,outfunc},
			   solnRules=evolveMode[L,m,kx,ky,kz,H,etamax];
			   nsolnRules=N[solnRules][[1]];
			   etaFinal=(psi/.nsolnRules)[[1,1,2]];
			   outfunc=Function[{chi},
					    Norm[psi[etaFinal,
						     chi]/.nsolnRules]];
			   outfunc]];

(* Samples from a probability distribution generated by evolving a mode *)
sampleDist[L_,m_,kx_,ky_,kz_,H_,etamax_]:=Module[{dist,sample},
	      dist=makeProbDist[L,m,kx,ky,kz,H,etamax];
	      sample=rejectionSample[dist,chimin,chimax,
				     normPsiMin,normPsiMax];
	      SaveIt[ToString[kx]<>"_"<>ToString[ky]<>"_"<>ToString[kz],
		     dist];
	      ClearMemory[];
  	      sample];

(* Evolves all modes up to kmax and then samples from each probability
   distribution. *)
sampleAllEvolvedModes[L_,m_,kmax_,H_,etamax_]:=Table[sampleDist[L,m,
								kx,ky,kz,
								H,etamax],
						     {kx,1,kmax},
						     {ky,1,kmax},
						     {kz,1,kmax}];

(* Generates a measurement of the field in a box after evolving the
   modes for some time.*)
MakeEvolvedFluctuations[L_,m_,kmax_,H_,etamax_]:=With[
    {chiK=Evaluate[sampleAllEvolvedModes[L,m,kmax,H,etamax]],
     aInverse=1/scaleFactorConformal[H,etamax]},
    Function[{x,y,z},
	     Evaluate[aInverse*Sum[chiK[[kx,ky,kz]]\
				   *Sin[kx*Pi*x/L]\
				   *Sin[ky*Pi*y/L]\
				   *Sin[kz*Pi*z/L],
				   {kx,1,kmax},
				   {ky,1,kmax},
				   {kz,1,kmax}]]]];

