(*
   SHO.m
   Time-stamp: <2014-04-20 23:36:16 (jonah)>
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

   This little module contains functions for a simple harmonic
   oscillator (SHO) in one dimension. We use it for a number of pieces
   of the code.

   For convenience, we work in natural units where hbar = c = G = 1.
   ======================================================================
   *)

(* Prevents kernel bloat *)
ClearMemory := Module[{},
		      Unprotect[In, Out];
		      Clear[In, Out];
		      Protect[In, Out];
		      ClearSystemCache[];
		     ];

(* The n^{th} energy eigenstate of the SHO without the time
   evolution. Depends on the quantum number n, the mass m, and the
   frpequency omega. The eigenstate is a function of the field
   amplitude phi. *)
ShoEigenstate[phi_,n_,m_,omega_]:=Sqrt[1/(2^n*Factorial[n])]\
    *((omega)/Pi)^(1/4)*Exp[-omega*phi^2/2]*HermiteH[n,Sqrt[omega]*phi];

(* Makes a function for a set n,m,omega that is the nth simple
harmonic oscillator eigenstate.*)
MakeShoEigenstate[n_,m_,omega_]:=Function[phi,ShoEigenstate[phi,n,m,omega]]

(* The energy of the n^{th} eigenstate of the SHO. Depends on the
   quantum number n and the frequency omega. *)
ShoEnergy[n_,omega_]:=omega*(n+(1/2));

(* Makes a function that returns the energy for the nth egeinstate of
a SHO with frequency omega.*)
MakeShoEnergy[omega_]:=Function[ShoEnergy[n,omega]]

(* Makes a probability distribution for the field amplitude phi if the
   harmonic oscillator is in the nth excited state. Depends also on
   the mass m and the frequency omega. *)
MakeShoProbability[n_,m_,omega_]:=ProbabilityDistribution[ShoEigenstate[phi,
									n,
									m,
								      omega]^2,
						  {phi,-Infinity,Infinity}];

(* Finds the norm square of the wave vector *)
waveNorm2[k_]:=Fold[#1+(#2)^2 &,0,k];

(* Finds the frequency for a harmonic oscillator with mode vector k
   and mass m in a box with side length L. *)
(*most general version*)
ShoFrequency[m_,L_,k_]:=Sqrt[(Pi/L)^2*Fold[#1+(#2)^2 &,0,k]+m^2];
(*version for 2d*)
ShoFrequency[m_,L_,kx_,ky_]:=ShoFrequency[m,L,{kx,ky}];
(*version for 3d*)
ShoFrequency[m_,L_,kx_,ky_,kz_]:=ShoFrequency[m,L,{kx,ky,kz}];


