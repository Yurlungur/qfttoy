(*
   rejection_sampling.m
   Time-stamp: <2014-04-20 15:28:28 (jonah)>
   Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

   This little module contains functions for doing rejection sampling
   for probability distributions.

   ======================================================================
   *)


(* Resets the random seed *)
SeedRandom[];

(* Uses rejection sampling to sample from a random distribution
assumed to be of the form f(x), which is always positive. f must have
domain [xmin,xmax] and range [ymin,ymax]. It does not need to be
normalized. *)
rejectionSample[f_,
		xmin_,xmax_,
		ymin_,ymax_]:=Module[{randX,pX,randY,underCurve=False},
				     While[Not[underCurve],
					   randX=RandomReal[{xmin,xmax}];
					   randY=RandomReal[{ymin,ymax}];
					   pX=f[randX];
					   underCurve=(randY <= pX)];
				     randX]
					   
